package com.lwjp.im;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

/**
 * A framework entrance.
 */
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class ImApplication {

    public static void main(String[] args) {
        // Default running as a application.
        SpringApplication.run(ImApplication.class, args);
    }
}
